// Storyline 3
//---------------------------------------------
// Compatible with: Build 3.5.16548.0
// Last updated: 11/12/18
// Notes: Any issues, speak with Gary

console.log('---- STORYLINE 3 GRUNT ----');

const options = {
  root: 'Pub/',
  scorm: 'SCORM/',
  includes: '_includes/',
  js: 'js/'
}

const g = Object.create(options);

/*----*/ /////// PUBLISHED FOLDER NAME ////////
g.folder = 'Folder name/'; // DONT FORGET TO INCLUDE THE '/' AT THE END OF THE FOLDER NAME
/*----*/ /////// PUBLISHED FOLDER NAME ////////

g.fullpath = g.root + g.scorm + g.folder;

g.jspath = g.includes + g.js;

var jsscripts = `
<script src="` + g.jspath + `jquery-1.12.3.min.js"></script>
<script src="` + g.jspath + `jquery-ui.min.js"></script>
<script src="` + g.jspath + `bgJS.min.js"></script>
</html>`;

var transparencyColor = `
var g_strBgColor = "";
//var g_strBgColor =`;

var transparencyWindow = `
var g_strWMode = "transparent";`;

var transparencyJS = `
//strWMode = "opaque";`;

var resetJS = `
function SCORM_ResetStatus(){
	var existingStatus = SCORM_GetStatus();
	if(existingStatus == 1){
	} else if(existingStatus == 2){
	} else if(existingStatus == 3){
	} else {
		WriteToDebug("In SCORM_ResetStatus");
		SCORM_ClearErrorInfo();
		return SCORM_CallLMSSetValue("cmi.core.lesson_status", SCORM_INCOMPLETE);
	}
}

function brokenResetStatusFunction(){
`;

var error = 0;

//some initial options error checking
var fs = require('fs');
if (!fs.existsSync(g.fullpath)) {
  console.log("Error: Published folder doesn't exist at: "+g.fullpath);
  error++;
} else {
  console.log('Published folder exists');
}

var bgjspath = g.includes + g.js + "bgJS.min.js";
if (!fs.existsSync(bgjspath)) {
  console.log("Error: bgJS.js doesn't exist at: "+bgjspath);
  error++;
} else {
  console.log('bgJS.js exists');
}

var imagesjsonpath = g.includes + g.js + "images.json";
if (!fs.existsSync(imagesjsonpath)) {
  console.log("Error: images.json doesn't exist at: "+imagesjsonpath);
  error++;
} else {
  console.log('images.json exists');
}

var json = JSON.parse(fs.readFileSync(imagesjsonpath, 'utf8'));

var objectKeysArray = Object.keys(json.images);
var imgpatharr = [];
objectKeysArray.forEach(function(objKey) {
    var imgsrc = json.images[objKey];

    imgpatharr.push(g.includes + 'images/' + imgsrc.src);
})

for(var i=0, len=imgpatharr.length; i < len; i++){
  var imgpath = imgpatharr[i];
  if(!fs.existsSync(imgpath)) {
    console.log("Error: Image doesn't exist at: "+imgpath);
    error++;
  } else {
    console.log('image exists');
  }
}

if(error) {
  console.log('-- There are '+error+' errors --');
  return false;
} else {
  console.log('No errors, run grunt');

  module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
      replace: {
        scripts: {
          src: [
            g.fullpath + 'story_flash.html',
            g.fullpath + 'index_lms_flash.html',
            g.fullpath + 'story_html5.html',
            g.fullpath + 'index_lms_html5.html'
          ],
          overwrite: true,
          replacements: [{
            from: '</html>',
            to: jsscripts
          }]
        },
        transparencycolor: {
          src: [
            g.fullpath + 'story_flash.html',
            g.fullpath + 'index_lms_flash.html'
          ],
          overwrite: true,
          replacements: [{
            from: 'var g_strBgColor',
            to: transparencyColor
          }]
        },
        transparencywindow: {
          src: [
            g.fullpath + 'story_flash.html',
            g.fullpath + 'index_lms_flash.html'
          ],
          overwrite: true,
          replacements: [{
            from: 'var g_strWMode = "window";',
            to: transparencyWindow
          }]
        },
        transparencyjs: {
          src: [
            g.fullpath + 'story_content/story.js'
          ],
          overwrite: true,
          replacements: [{
            from: 'strWMode = "opaque";',
            to: transparencyJS
          }]
        },
        configs: {
          src: [
            g.fullpath + 'lms/Configuration.js'
          ],
          overwrite: true,
          replacements: [{
            from: 'var REVIEW_MODE_IS_READ_ONLY = true;',
            to: 'var REVIEW_MODE_IS_READ_ONLY = false;'
          }]
        },
        resetstatus: {
          src: [
            g.fullpath + 'lms/SCORMFunctions.js'
          ],
          overwrite: true,
          replacements: [{
            from: "function SCORM_ResetStatus(){",
            to: resetJS
          }]
        },
        backgroundBodyID: {
			src: [
				g.fullpath + 'story_flash.html',
				g.fullpath + 'index_lms_flash.html',
				g.fullpath + 'story_html5.html',
				g.fullpath + 'index_lms_html5.html'
			],
			overwrite: true,
			replacements: [{
				from:'<body',
				to:'<body id="htmlBackground"'
			}]
		}
      },

      copy: {
        includes: {
          cwd: g.includes,
          src: ['**/*', '!**/*.db'],
          dest: g.root + g.scorm + g.folder + g.includes,
          expand: true
        }
      }

    });

    grunt.registerTask('default', [
      'replace:scripts',
      'replace:transparencycolor',
      'replace:transparencywindow',
      'replace:transparencyjs',
      'replace:configs',
      'replace:resetstatus',
      'replace:backgroundBodyID',
      'copy:includes',
    ]);

  };
}